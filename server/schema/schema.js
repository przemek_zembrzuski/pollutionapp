const { GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLList, GraphQLID } = require('graphql');
const fetch = require('node-fetch');
require('dotenv').config()


const cityNamesType = new GraphQLObjectType({
    name: 'citiesNames',
    fields() {
        return {
            name: { type: GraphQLString },
        }
    }
})

const cityType = new GraphQLObjectType({
    name: 'city',
    fields(){
        return {
            cityName: { type: GraphQLString},
            cityCoordinates: {
                 type: cityCoordinatesType,
                 resolve(parent){
                     return {
                        lat: parent.cityCoordinates[1],
                        lon: parent.cityCoordinates[0],
                     } 
                 }
            }
        }
    }
})
const cityCoordinatesType = new GraphQLObjectType({
    name: 'cityCoordinates',
    fields(){
        return {
            lat: { type: GraphQLString },
            lon: { type: GraphQLString }
        }
    }
})
const cityPollutionType = new GraphQLObjectType({
    name: 'cityPollution',
    fields() {
        return {
            id: { type: GraphQLID },
            stationName: { type: GraphQLString },
            lat: { type: GraphQLString },
            lon: { type: GraphQLString },
            city: { 
                type: cityType,
                async resolve(parent){
                    const resolve = await fetch(encodeURI(`https://api.mapbox.com/geocoding/v5/mapbox.places/pl&${parent.cityName}.json?access_token=${process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}`)) 
                    const json = await resolve.json();
                    return {
                        cityName: parent.cityName,
                        cityCoordinates: json.features[0].center
                    }
                }

            },
            street: { type: GraphQLString },
            param: {
                type: new GraphQLList(pollutionParameter),
                async resolve(parent) {
                    const resolve = await fetch(encodeURI(`http://api.gios.gov.pl/pjp-api/rest/station/sensors/${parent.id}`));
                    const json = await resolve.json();
                    const dataObj = json.map(element => ({
                        id: element.id,
                        paramName: element.param.paramName,
                        paramFormula: element.param.paramFormula
                    }))
                    return dataObj
                }
            }
        }
    }
})
const pollutionParameter = new GraphQLObjectType({
    name: 'PollutionParameter',
    fields() {
        return {
            id: { type: GraphQLID },
            paramName: { type: GraphQLString },
            paramFormula: { type: GraphQLString },
            pollution: {
                type: PollutionValueType,
                async resolve(parent) {
                    const resolve = await fetch(encodeURI(`http://api.gios.gov.pl/pjp-api/rest/data/getData/${parent.id}`));
                    const json = await resolve.json();
                    return json.values.find(element => element.value)
                }
            }
        }
    }
})

const PollutionValueType = new GraphQLObjectType({
    name: 'PollutionValue',
    fields() {
        return {
            date: { type: GraphQLString },
            value: { type: GraphQLString }
        }
    }
})
const root = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        cityNames: {
            type: new GraphQLList(cityNamesType),
            async resolve() {
                const resolve = await fetch(encodeURI('http://api.gios.gov.pl/pjp-api/rest/station/findAll'));
                const json = await resolve.json();
                const filteredCitiesName = json.map(station => station.city.name)
                    .filter((element, index, array) => index === array.indexOf(element))
                    .map(element => ({ name: element }))
                return filteredCitiesName
            }
        },
        cityPollution: {
            type: new GraphQLList(cityPollutionType),
            args: { city: { type: GraphQLString } },
            async resolve(parent, args) {
                const resolve = await fetch(encodeURI('http://api.gios.gov.pl/pjp-api/rest/station/findAll'));
                const json = await resolve.json();
                const stations = json.filter(element => element.city.name.toLowerCase() === args.city.toLowerCase())
                    .map(element => ({
                        id: element.id,
                        stationName: element.stationName,
                        lat: element.gegrLat,
                        lon: element.gegrLon,
                        cityName: element.city.name,
                        street: element.addressStreet
                    }))
                return stations
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: root
})