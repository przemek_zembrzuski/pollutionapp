const express = require('express');
const app = express();
const cors = require('cors');
const graphqlHTTP = require('express-graphql');

const schema = require('./schema/schema');

app.use(cors())

app.use('/graphql', graphqlHTTP({
    schema
}))


app.listen(4400,()=>{
    console.log('listening')
})


 




