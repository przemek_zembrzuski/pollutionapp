import gql from 'graphql-tag';

const getAllCityNames = ()=>{
    return gql`{
        cityNames{
            name
        }
    }`

}
const getCityPollution = (city)=>{
    return gql`{
        cityPollution(city: "${city}") {
          lat
          lon
          city{
              cityName,
              cityCoordinates{
                  lat,
                  lon
              }
          }
          param {
            paramName
            paramFormula
            pollution {
              value
            }
          }
        }
      }`

}


export {
    getAllCityNames,
    getCityPollution
}