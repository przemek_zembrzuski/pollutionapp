import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Store from './Redux/Store'
import './App.css';
import Autocomplete from './Components/Autocomplete/Autocomplete';
import Map from './Components/Map/Map'
import Pollution from './Components/Pollution/Pollution'



class App extends Component {
  render() {
    return (
      <Provider store={Store}>
          <div id="container">
            <Autocomplete />
            <Pollution />
            <Map />
          </div>
      </Provider>

    )
  }
}

export default App;
