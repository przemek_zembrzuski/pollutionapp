import React, { Component } from 'react';
import './Autocomplete.css';

import { connect } from 'react-redux';

import { setCityName } from '../../Redux/Actions/Actions';

import client from '../../Apollo/Client';
import { getAllCityNames }  from '../../Apollo/Queries'

class Autocomplete extends Component {
    constructor() {
        super();
        this.state= {
            inputValue: '',
            autocompleteHelper: '',
            cityNames: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        const resolve = await client.query({query: getAllCityNames()});
        this.state.cityNames = resolve.data.cityNames.map(citiesObj => citiesObj.name);
    }
    firstLetterUpperCase(string) {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }
    handleChange(e) {
        const inputValue = e.target.value;
        let cityHelper;
        if (inputValue) {
            const inputWordLength = inputValue.length;
            cityHelper = this.state.cityNames.filter(element => {
                return element.toLowerCase().slice(0, inputWordLength) === inputValue.toLowerCase()
            })[0]
        }
        this.setState({
            inputValue:this.firstLetterUpperCase(inputValue),
            autocompleteHelper: cityHelper ? this.firstLetterUpperCase(cityHelper) : ''
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        if (this.state.autocompleteHelper) {
            this.setState({
                inputValue:this.state.autocompleteHelper
            })
            this.props.setCityName(this.state.autocompleteHelper)
        }
    }
    render() {
        return (
            <div id="autocomplete">
                <form onSubmit={this.handleSubmit}>
                    <input className="autocompleteInputHelper" value={this.state.autocompleteHelper} type="text" disable="true" />
                    <input type="text" className="autocompleteInput" value={this.state.inputValue} onChange={this.handleChange} placeholder="Podaj miasto" />
                    <button type="submit" hidden="true"></button>
                </form>
            </div>
        );
    }
}


const mapDispatchToProps = {
    setCityName
}


export default connect(null, mapDispatchToProps)(Autocomplete);