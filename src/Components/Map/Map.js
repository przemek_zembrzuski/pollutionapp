import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import { connect } from 'react-redux';
import './Map.css';






class Map extends Component {
    constructor() {
        super();
        this.map = undefined;
        mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN
    }
    randomNumber(num = 1){
        if(num === 0 ){
            return (num + 1) * Math.random()
        }
        return num * Math.random()
    }
    correctPollutionValue(pollutionValue){
        if(pollutionValue < 0.5 ) {
            return pollutionValue * 100
        }
        if(pollutionValue < 15 ) {
            return pollutionValue * 15
        }
        if(pollutionValue < 100){
            return pollutionValue * 1.5
        } 
        return pollutionValue
    } 
    pollutionColorIndicator(pollution){
        if(pollution > 100){
            return `rgba(255,0,0,.7)`
        }
        return `rgba(0,255,0,.7)`

    }   

    createLayers(){
        return this.props.cityPollution.map((element,index)=>({
            "id": "Points"+this.randomNumber(index),
            "type": "circle",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "Point",
                                "coordinates": [
                                    element.lon,
                                    element.lat
                                ]
                            }
                        }
                    ]
                }
            },
            "paint": {
                "circle-color": this.pollutionColorIndicator(this.correctPollutionValue(this.props.pollutionType.value)),
                "circle-radius": {
                    "stops": this.defineRadiusOnZoom(12,this.correctPollutionValue(this.props.pollutionType.value)) 
                },
                "circle-blur": 0.5
            }
        }))
    }
    componentDidMount() {
        this.map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            center: [19.7065364, 52.5463446],
            zoom: 12,
            pitch: 90,
            scrollZoom: false
        });
    }
    mapAnimation(){
        for(let element of this.map.getStyle().layers){
            if(element.id.includes('Points')){
                this.map.removeLayer(element.id)
            }
        }
        this.createLayers().map(element=>
            this.map.addLayer(element,this.getFirstSymbolId())
        )
        this.map.flyTo({center: [this.props.cityPollution[0].city.cityCoordinates.lon,this.props.cityPollution[0].city.cityCoordinates.lat], zoom: 13})
    }
    componentDidUpdate(){
        if(this.props.cityPollution.length){
            this.mapAnimation()
        }
    }
    defineRadiusOnZoom(minZoom,radius){
        //zoom = min zoom definde on the map
        // radius = radius of the circle 

        const radiusOnZoomsArray = [];
        for(let i = 0; i <= minZoom; i++){
            let radiusOnZoom;
            if(i===0){
                radiusOnZoom = [0, 0]
            }else{
                radiusOnZoom = [i, radius/(minZoom-i+1)]
            }
            radiusOnZoomsArray.push(radiusOnZoom)
        }
        return radiusOnZoomsArray
    }
    getFirstSymbolId(){
        return this.map.getStyle().layers.filter(layer => layer.type === 'symbol')[0].id
    }
    render() {
        return (
            <div id='map'></div>
        )
    }

}
const mapStateToProps = (state)=>({
    cityName: state.cityName,
    cityPollution: state.cityPollution,
    pollutionType: state.pollutionType
})
export default connect(mapStateToProps)(Map);



    


