import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Pollution.css'

import client from '../../Apollo/Client';
import { getCityPollution } from '../../Apollo/Queries';

import { setCityPollution, setPollutionType } from '../../Redux/Actions/Actions'

class Pollution extends Component {

    async componentDidUpdate() {
        const resolve = await client.query({
            query: getCityPollution(this.props.cityName)
        })
        if (!this.props.cityPollution.length || (this.props.cityName !== this.props.cityPollution[0].city.cityName)) {
            this.props.setCityPollution(resolve.data.cityPollution)
        }
        this.props.setPollutionType(this.averageData(this.props.cityPollution)[Object.keys(this.averageData(this.props.cityPollution))[0]])

    }

    averageData(data) {
        if (data.length) {
            const pollutionDataArrays = this.props.cityPollution.map(element => element.param);
            const arrayOfPollutionData = [].concat(...pollutionDataArrays);
            const pollutionDataSorted = arrayOfPollutionData.reduce((a, b) => {
                a[b.paramFormula] = {
                    values: a[b.paramFormula] ? [...a[b.paramFormula].values, b.pollution.value] : [b.pollution.value],
                    paramName: b.paramName
                }
                return a
            }, {})
            const averagePollutionData = Object.keys(pollutionDataSorted).map(key => {
                return {
                    paramFormula:key,
                    paramName: pollutionDataSorted[key].paramName,
                    value: (+pollutionDataSorted[key].values.reduce((a, b) => {
                        return  +a + +b 
                    }) / pollutionDataSorted[key].values.length).toFixed(5)
                }
            })
            return averagePollutionData
        }
    }
    handleClick(pollutionType){
        this.props.setPollutionType(pollutionType)
    }

    render() {
        return (
            <div id="pollution">
                {
                    this.averageData(this.props.cityPollution) &&
                    Object.keys(this.averageData(this.props.cityPollution)).map(key => {
                        return (
                            <div key={key} className="pollutionInfoContainer" onClick={()=>this.handleClick(this.averageData(this.props.cityPollution)[key])}>
                                <h2>{this.averageData(this.props.cityPollution)[key].paramFormula}</h2>
                                <span>
                                    {this.averageData(this.props.cityPollution)[key].paramName }
                                </span>
                                <p>{this.averageData(this.props.cityPollution)[key].value}</p>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

}
const mapStateToProps = (state) => ({
    cityName: state.cityName,
    cityPollution: state.cityPollution
})

const mapDispatchToProps = {
    setCityPollution,
    setPollutionType
}
export default connect(mapStateToProps, mapDispatchToProps)(Pollution);






