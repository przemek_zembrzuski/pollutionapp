const getCity = (cityObj) => ({
    type: 'GET_CITY',
    payloads: cityObj
})

const setCityName = (cityName) => ({
    type: 'SET_CITY_NAME',
    payloads: cityName
})

const setCityPollution = (cityPollution) => ({
    type: 'SET_CITY_POLLUTION',
    payloads: cityPollution
})
const setPollutionType = (pollutionType) => ({
    type: 'SET_POLLUTION_TYPE',
    payloads: pollutionType
})

export {
    getCity,
    setCityName,
    setCityPollution,
    setPollutionType
}