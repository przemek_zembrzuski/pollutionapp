const defaultState = {
    cityName: '',
    cityPollution: [],
    pollutionType: {}
}

const cities = (state = defaultState, action) => {

    switch (action.type) {
        case 'GET_CITY':
            return { ...state, city: action.payloads }
        case 'SET_CITY_NAME':
            return { ...state, cityName: action.payloads }
        case 'SET_CITY_POLLUTION':
            return { ...state, cityPollution: action.payloads }
        case 'SET_POLLUTION_TYPE':
            return { ...state, pollutionType: action.payloads }
        
        default:
            return state;
    }

}

export default cities;