import { createStore } from 'redux';

import cities from './Reducers/Cities'; 


const Store = createStore(
    cities, /* preloadedState, */
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );


  export default Store;